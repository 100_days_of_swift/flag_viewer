//
//  FlagDetailViewController.swift
//  FlagViewer
//
//  Created by Hariharan S on 04/05/24.
//

import UIKit

class FlagDetailViewController: UIViewController {
    
    // MARK: - IBOutlet

    @IBOutlet private weak var imageView: UIImageView!
    
    // MARK: - Property
    
    var selectedFlagName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavBar()
        self.configureImageView()
    }
}

// MARK: - Private Methods

private extension FlagDetailViewController {
    func configureNavBar() {
        self.title = self.selectedFlagName
        self.navigationItem.largeTitleDisplayMode = .never
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .action,
            target: self,
            action: #selector(self.shareButtonTapped)
        )
    }
    
    func configureImageView() {
        self.imageView.layer.borderWidth = 2
        self.imageView.layer.cornerRadius = 16
        self.imageView.layer.borderColor = UIColor.gray.cgColor
        
        guard let image = self.selectedFlagName
        else {
            return
        }
        self.imageView.image = UIImage(named: "\(image).png")
        self.imageView.setImageSizeToFit()
    }
    
    @objc func shareButtonTapped() {
        guard let image = self.imageView.image?.jpegData(compressionQuality: 1.0)
        else {
            fatalError("No image found")
        }
        
        let activityVC = UIActivityViewController(
            activityItems: [image, self.selectedFlagName as Any],
            applicationActivities: []
        )
        activityVC.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        self.present(activityVC, animated: true)
    }
}

extension UIImageView {
    func setImageSizeToFit() {
        if let image = self.image {
            let aspectRatio = image.size.width / image.size.height
            let newWidth = min(self.frame.size.width, image.size.width)
            let newHeight = newWidth / aspectRatio
            self.frame.size = CGSize(width: newWidth, height: newHeight)
        }
    }
}
