//
//  FlagTableViewController.swift
//  FlagViewer
//
//  Created by Hariharan S on 04/05/24.
//

import UIKit

class FlagTableViewController: UITableViewController {

    private var flagNames: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavBar()
        self.loadCountriesFlag()
    }
}

// MARK: - Private Methods

private extension FlagTableViewController {
    func configureNavBar() {
        self.title = "Flag Viewer"
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func loadCountriesFlag() {
        let fileManager = FileManager.default
        guard let path = Bundle.main.resourcePath 
        else {
            return
        }
        
        do {
            let flagsData = try fileManager.contentsOfDirectory(atPath: path)
            flagsData.forEach { flagName in
                if flagName.hasSuffix(".png"), !flagName.hasPrefix("App") {
                    let countryName = flagName.replacingOccurrences(of: ".png", with: "")
                    self.flagNames.append(countryName)
                }
            }
            self.flagNames.sort()
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }
}

// MARK: - UITableViewDelegate Conformance

extension FlagTableViewController {
    override func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        guard let flagDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "FlagDetailViewController") as? FlagDetailViewController 
        else {
            return
        }
        flagDetailVC.selectedFlagName = self.flagNames[indexPath.row]
        self.navigationController?.pushViewController(flagDetailVC, animated: true)
    }
}

// MARK: - UITableViewDataSource Conformance

extension FlagTableViewController {
    override func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        self.flagNames.count
    }
    
    override func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "Flag",
            for: indexPath
        )
        cell.textLabel?.text = self.flagNames[indexPath.row]
        return cell
    }
}
